var name = "It"; /* Name of snake */

window.onload = function() {
	var box = document.getElementById("box");

	var dirs = {
		none: -1,
		left: 0,
		right: 1,
		up: 2,
		down: 3,
		opposite: function(x) {
			switch(x) {
			case dirs.left:
			case dirs.up:
				return x + 1;
			case dirs.down:
			case dirs.right:
				return x - 1;
			default:
				return dirs.none;
			}
		}
	};
	var direction = dirs.none;

	var size = 30;
	var interval = 100;
	var pts_ = 0;
	Object.defineProperty(window, "pts", {
		get: function() {
			return pts_;
		},
		set: function(val) {
			pts_ = val;
			document.getElementById("pts").innerHTML = pts_;
		}
	}); /* Points */

	/* Resets */
	function reset() {
		box.style.top =  "90px"; 
		box.style.left = "90px";
		direction = dirs.none;
		interval = 100;
		pts = 0;
	}
	reset();

	/***********************************
	 * Popups                          *
	 *                                 *
	 * Thing that shows You Lose, etc. *
	 ***********************************
	 */

	var currentDialog = null;

	function popup(msg, submsg, cb) {
		document.getElementById("screen").style.filter = "brightness(70%)";
		var card = document.createElement("div");	
		card.classList.add("card", "popup");
		card.innerHTML = "<p class=\"title\">" + msg + "</p>";
		if(submsg) {
			card.innerHTML += "<p class=\"text\">" + submsg + "</p>";
		}

		var obj = {};
		obj.title = msg;
		obj.text = submsg;
		obj.close = function() {
			try { obj.cb(); } catch(err) { console.error("uncaught exception: " + err); }
			card.remove();
			document.getElementById("screen").style.filter = "none";
			currentDialog = null;
		}
		obj.cb = cb;
		currentDialog = obj;

		var closebtn = document.createElement("button");
		closebtn.innerHTML = "&times;"
		closebtn.classList.add("popup-close");
		closebtn.onclick = function() { obj.close() };
		card.appendChild(closebtn);

		document.body.appendChild(card);
		return obj;
	}

	isOn = function isOn() { // is the game not paused?
		return currentDialog === null;
	}

	/******************************************************************************/

	function pause() {
		if(isOn()) {
			popup("Paused");
		}
	}

	function lose() {
		popup(name + " Died", name + " hit the wall", reset);
	}

	function move() {
		if(isOn()) {
			switch(direction) {
				case dirs.left:
					box.style.left = box.style.left.slice(0, -2) - size + "px";
					box.style.borderWidth = "0 0 0 2px";
					break;
				case dirs.right:
					box.style.left = box.style.left.slice(0, -2) - (-size) + "px";
					box.style.borderWidth = "0 2px 0 0";
					break;
				case dirs.up:
					box.style.top = box.style.top.slice(0, -2) - size + "px";
					box.style.borderWidth = "2px 0 0 0";
					break;
				case dirs.down:
					box.style.top = box.style.top.slice(0, -2) - (-size) + "px";
					box.style.borderWidth = "0 0 2px 0";
					break;
			}
		}
	}

	function changeDirection(dir) {
		if(isOn() && direction !== dirs.opposite(dir)) {
			direction = dir;
		}
	}

	/* When key pressed */
	Mousetrap.bind('up', function() { changeDirection(dirs.up); });
	Mousetrap.bind('down', function() { changeDirection(dirs.down); });
	Mousetrap.bind('left', function() { changeDirection(dirs.left); });
	Mousetrap.bind('right', function() { changeDirection(dirs.right); });
	Mousetrap.bind('space', function() {
		if(isOn()) {
			pause();
		} else {
			currentDialog.close();
		}
	});

	function spawnCoin() {
		var t = Math.floor(Math.random() * Math.floor(window.innerHeight / (2*size)));
		var l = Math.floor(Math.random() * Math.floor(window.innerWidth / (2*size)));
		var top = t * 2*size + 30;
		var left = t * 2*size + 30;
		var coin = document.getElementById("coin");
		if(coin.style.top !== top + "px" || coin.style.left !== left + "px") {
			coin.style.top = top + "px";
			coin.style.left = left + "px";
		}
	}

	/* Mainloop */
	function main() {
		move();

		/* You Lose */
		if((Number(box.style.top.slice(0, -2)) < 0 || Number(box.style.left.slice(0, -2)) < 0 || Number(box.style.left.slice(0, -2)) > window.innerWidth - 30 || Number(box.style.top.slice(0, -2)) > window.innerHeight - 30) && isOn()) {
			lose();
		}

		/* Coin */
		if(box.style.top == coin.style.top && box.style.left == coin.style.left) {
			spawnCoin();
			pts++;
		}
	}

	function setup() {
		spawnCoin();
		var id = setInterval(main, interval);
	}

	setup();
}
